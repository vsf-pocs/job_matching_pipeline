import json
import boto3


client = boto3.client("dynamodb")


def lambda_handler(event, context):
    data = client.get_item(TableName="job_titles", Key={"title": {"S": "Chancellor"}})

    response = {
        "statusCode": 200,
        "body": data,
        "headers": {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
        },
    }

    return response
