[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)


# job_matching_pipeline

Deploys an API that receives a real-world job description title and returns all official job codes from the [Occupational Information Network (O*NET)](https://www.onetonline.org/). 

For example:
 - input: "(Senior) Data Scientist, Consumer (m/f/d)"
 - output: [15-2051.00](https://www.onetonline.org/link/summary/15-2051.00)
 
 With this API, it's possible to categorize real-world job descriptions by checking its job title, that usually is not well formatted in accordance with O\*NET official list. O*NET can also give more information about the specific role for this job title.

**Table of Contents**
- [Tech Stack](#tech-stack)
- [Installation](#installation)
- [Usage](#usage)
- [Project status / Roadmap](#project-status--roadmap)

## Tech Stack
- IaC with AWS CloudFormation / CDK
- S3 to retrieve raw data from O*NET
- AWS Data Pipeline 
    - EMR activity: from S3 to DynamoDB
- DynamoDB to store all official job codes from O*NET
- AWS lambda + API Gateway

## Installation
pipenv was used for dependency management: 
```shell script
pipenv install
```

## Usage
Instructions to deploy the solution on AWS...

## Project status / Roadmap
This is an ongoing project...

- IaC
    - [X] _add DynamoDB to the template_
    - [X] _add Data Pipeline to the template_
    - [X] _configure GitLab CI/CD_
    - [X] _run sam deploy with stack changes_
    - [ ] add lambda to the template
    - [ ] add API Gateway to the template
    - [ ] implement CDK to compose templates
- AWS Lambda
    - [ ] receive a job title from GET parameter 
    - [ ] normalize title before searching for official code
- Other
    - [ ] add image of the architercture to README.md
    - [ ] add description of how IaC and pipeline work
- Issues
    - [ ] create new data pipeline without immediately activating it